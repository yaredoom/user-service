# For assistance debugging failures

* The pact files have been stored locally in the following temp directory:
    /Users/yaredwondimu/sondermind/gitlab/contract-testing-poc/user-service/tmp/pacts

* The requests and responses are logged in the following log file:
    /Users/yaredwondimu/sondermind/gitlab/contract-testing-poc/user-service/log/pact.log

* Add BACKTRACE=true to the `rake pact:verify` command to see the full backtrace

* If the diff output is confusing, try using another diff formatter.
  The options are :unix, :embedded and :list

    Pact.configure do | config |
      config.diff_formatter = :embedded
    end

  See https://github.com/pact-foundation/pact-ruby/blob/master/documentation/configuration.md#diff_formatter for examples and more information.

* Check out https://github.com/pact-foundation/pact-ruby/wiki/Troubleshooting

* Ask a question on stackoverflow and tag it `pact-ruby`


The following changes have been made since the previous distinct version of this pact, and may be responsible for verification failure:

# Diff between versions 0.0.0 and 0.0.1 of the pact between BlogService and UserService

The following changes were made 3 minutes ago (Tue 18 Oct 2022, 10:41pm +00:00)

     {
       "response": {
         "headers": {
-          "Content-Type": "application/json; charset=utf-8"
         }
       }
     },

## Links

pact-version:
  title: Pact
  name: Pact between BlogService (0.0.1) and UserService
  href: http://localhost/pacts/provider/UserService/consumer/BlogService/version/0.0.1
comparison-pact-version:
  title: Pact
  name: Pact between BlogService (0.0.0) and UserService
  href: http://localhost/pacts/provider/UserService/consumer/BlogService/version/0.0.0
