Pact.provider_states_for 'BlogService' do
  provider_state 'a user with id 100 exists' do
    set_up do
      User.create(id: 100, name: 'Contract Tester', email: 'Contract.TESTER@example.com')
    end

    tear_down do
      User.find_by(id: 100)&.destroy
    end
  end
end