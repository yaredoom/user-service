A Provider user-service for Contract-Testing Demo
Providers a GET /users/{id} api

# Verify contract using pact

This application should comply to a contract with the blog application, for
providing user data. To verify that a change does not break the contract run.
You could link the contract directly as a file if you have it on your local machine.

```
bundle exec rails pact:verify
```

The pacts that will be verified by the `pact:verify` task are configured in the pact_helper.rb file `spec/support/service_consumers/pact_helper.rb`

Verifying Pacts Documentation:
* https://docs.pact.io/implementation_guides/ruby/verifying_pacts